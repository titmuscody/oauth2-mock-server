#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use] extern crate rocket;
#[macro_use] extern crate rocket_contrib;

use std::collections::HashMap;

use rocket::response::{Redirect, Response, Responder};
use rocket::request::Request;
use rocket::http::{Status};
use rocket_contrib::json::{Json, JsonValue};
use url::Url;
use uuid::Uuid;

type DB = std::sync::Arc<std::sync::RwLock<HashMap<u64, DBEntry>>>;
type TokenDB = std::sync::Arc<std::sync::RwLock<HashMap<String, String>>>;

struct DBEntry {
    // redirect_uri: String,
    client_id: String,
    user: String,
}

enum Authorization {
    Bearer(String),
}

#[derive(Debug)]
enum AuthorizeError {
    InvalidResponseType,
    InvalidRedirectUri(url::ParseError),
}

impl From<url::ParseError> for AuthorizeError {
    fn from(error: url::ParseError) -> Self {
        AuthorizeError::InvalidRedirectUri(error)
    }
}

impl<'r> Responder<'r> for AuthorizeError {
    fn respond_to(self, _: &Request) -> rocket::response::Result<'r> {
        let res = json!({"code": format!("{:?}", self)}).to_string();
        Response::build()
            .sized_body(std::io::Cursor::new(res))
            .status(Status::BadRequest)
            .ok()
    }
}

#[get("/authorize?<redirect_uri>&<response_type>&<client_id>&<user>&<_state>")]
fn index(redirect_uri: String, response_type: String, client_id: String, _state: Option<u64>, user: Option<String>, db: rocket::State<DB>) -> Result<Redirect, AuthorizeError> {
    let email = user.unwrap_or("user@gmail.com".to_string());
    if response_type != "code" {
        return Err(AuthorizeError::InvalidResponseType)
    }
    let entry_id = rand::random::<u64>();
    let mut url: Url = Url::parse(&redirect_uri)?;

    let entry = DBEntry{
        // redirect_uri: params.redirect_uri,
        client_id: client_id,
        user: email,
    };
    db.write().unwrap().insert(entry_id, entry);

    let new_query = match url.query() {
        Some("") => format!("&code={}", entry_id),
        Some(val) => format!("{}&code={}", val, entry_id),
        None => format!("&code={}", entry_id),
    };
    url.set_query(Some(&new_query));

    Ok(Redirect::to(url.to_string()))
}

#[derive(Debug)]
enum TokenError {
    InvalidAuthorizationCode,
    InvalidCode,
    Auth,
}

impl<'r> Responder<'r> for TokenError {
    fn respond_to(self, _: &Request) -> rocket::response::Result<'r> {
        let res = json!({"code": format!("{:?}", self)}).to_string();
        Response::build()
            .sized_body(std::io::Cursor::new(res))
            .status(Status::BadRequest)
            .ok()
    }
}

#[derive(serde::Deserialize)]
struct TokenPayload {
    grant_type: String,
    code: u64,
    client_id: String,
    // client_secret: String,
    // redirect_uri: String,
}

#[post("/token", data = "<data>")]
fn token(db: rocket::State<DB>, token_db: rocket::State<TokenDB>, data: Json<TokenPayload>) -> Result<JsonValue, TokenError> {
    if data.grant_type != "authorization_code" {
        return Err(TokenError::InvalidCode)
    }
    match db.read().unwrap().get(&data.code) {
        None => Err(TokenError::InvalidAuthorizationCode),
        Some(entry) => {
            if data.client_id != entry.client_id {
                return Err(TokenError::Auth)
            }
            let uuid = Uuid::new_v4().to_string();
            token_db.write().unwrap().insert(uuid.to_string(), entry.user.clone());
            Ok(json!({"access_token": uuid}))
        }
    }
}

#[derive(Debug)]
enum AuthorizationError {
    Invalid,
    BadCount,
    Missing,
    BadCode,
}
impl<'a ,'r> rocket::request::FromRequest<'a, 'r> for Authorization {
    type Error = AuthorizationError;

    fn from_request(request: &'a Request<'r>) -> rocket::request::Outcome<Self, Self::Error> {
        let keys: Vec<_> = request.headers().get("Authorization").collect();
        match keys.len() {
            0 => rocket::Outcome::Failure((Status::BadRequest, AuthorizationError::Missing)),
            1 => {
                let val = keys[0];
                if val.starts_with("Bearer ") {
                    return rocket::Outcome::Success(Authorization::Bearer(val[7..].to_string()))
                }
                rocket::Outcome::Failure((Status::BadRequest, AuthorizationError::Invalid))
            }
            _ => rocket::Outcome::Failure((Status::BadRequest, AuthorizationError::BadCount)),
        }
    }
}
#[get("/userinfo")]
fn userinfo(token_db: rocket::State<TokenDB>, auth: Authorization) -> Result<JsonValue, AuthorizationError> {
    let auth_val = match auth {
        Authorization::Bearer(val) => val,
    };
    match token_db.read().unwrap().get(&auth_val) {
        None => Err(AuthorizationError::BadCode),
        Some(email) => Ok(json!({"email": email}))
    }
}

fn main() {

    let db: DB = std::sync::Arc::new(std::sync::RwLock::new(HashMap::new()));
    let token_db: TokenDB = std::sync::Arc::new(std::sync::RwLock::new(HashMap::new()));

    rocket::ignite()
        .mount("/", routes![index, token, userinfo])
        .manage(db)
        .manage(token_db)
        .launch();
}
