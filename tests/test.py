import unittest
import requests
from urllib.parse import urlparse, parse_qs

client_id = 'test_client_id'
client_secret = 'test_client_secret'
redirect_url = 'http://localhost/vote-api'

class TestAuthorize(unittest.TestCase):
    def test_redirect_uri(self):
        res = requests.get('http://localhost:9000/authorize?redirect_uri={}&response_type=invalid&client_id={}'.format('bogus:url', client_id), allow_redirects=False)
        self.assertEqual(res.status_code, 400, 'should get error when invalid url is given')
    def test_response_types(self):
        res = requests.get('http://localhost:9000/authorize?redirect_uri={}&response_type=invalid&client_id={}'.format(redirect_url, client_id), allow_redirects=False)
        self.assertEqual(res.status_code, 400, 'should get error when incorrect response_type specified')
    def test_code_is_always_unique(self):
        first_code = self.get_code()
        second_code = self.get_code()
        self.assertNotEqual(first_code, second_code, 'repeat queries should return different codes')

    def get_code(self, redirect_uri=redirect_url, client_id=client_id):
        res = requests.get('http://localhost:9000/authorize?redirect_uri={}&response_type=code&client_id={}'.format(redirect_url, client_id), allow_redirects=False)
        params = parse_qs(urlparse(res.headers['location']).query)
        return params['code'][0]


class TestToken(unittest.TestCase):
    def setUp(self):
        res = requests.get('http://localhost:9000/authorize?redirect_uri={}&response_type=code&client_id={}'.format(redirect_url, client_id), allow_redirects=False)
        params = parse_qs(urlparse(res.headers['location']).query)
        self.code = int(params['code'][0])

    def create_payload(self, code=None, grant_type='authorization_code', client_id=client_id, client_secret=client_secret, redirect_uri=redirect_url):
        if not code:
            code = self.code
        return {
            'grant_type': grant_type,
            'code': code,
            'client_id': client_id + 'invalid',
            'client_secret': client_secret,
            'redirect_uri': redirect_url,
        }

    def test_incorrect_client_id(self):
        data = self.create_payload(client_id=client_id + 'invalid')
        res = requests.post('http://localhost:9000/token', json=data)
        self.assertEqual(res.status_code, 400, 'getting a token with a different clientid should fail')

    def test_incorrect_redirect_uri(self):
        data = self.create_payload(redirect_uri=redirect_url + '/invalid')
        res = requests.post('http://localhost:9000/token', json=data)
        self.assertEqual(res.status_code, 400, 'getting a token with a different redirect_uri should fail')

    def test_incorrect_grant_type(self):
        data = self.create_payload(grant_type='authorization_code' + '_invalid')
        res = requests.post('http://localhost:9000/token', json=data)
        self.assertEqual(res.status_code, 400, 'getting a token with wrong code should fail')


# class TestUserInfo(unittest.TestCase):
#     pass

class TestAll(unittest.TestCase):

    def test_login(self):
        custom_email = 'custom_user@hotmail.com'
        res = requests.get('http://localhost:9000/authorize?redirect_uri={}&response_type=code&client_id={}&user={}'.format(redirect_url, client_id, custom_email), allow_redirects=False)
        params = parse_qs(urlparse(res.headers['location']).query)
        self.assertIn('code', params, msg='redirect should have provided a code in the redirect')
        self.assertEqual(len(params['code']), 1)
        code = int(params['code'][0])

        data = {
            'grant_type': 'authorization_code',
            'code': code,
            'client_id': client_id,
            'client_secret': client_secret,
            'redirect_uri': redirect_url,
        }
        res = requests.post('http://localhost:9000/token', json=data)
        self.assertEqual(res.status_code, 200, 'request should of succeeded {}'.format(res.text))
        access_token = res.json()['access_token']

        res = requests.get('http://localhost:9000/userinfo', headers={'Authorization': 'Bearer {}'.format(access_token)})
        self.assertEqual(res.status_code, 200, 'userinfo request should have succeeded: {}'.format(res.text))
        self.assertEqual(res.json()['email'], custom_email)


if __name__ == '__main__':
    unittest.main()