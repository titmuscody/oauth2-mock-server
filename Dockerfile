FROM rustlang/rust:nightly as cargo-build
WORKDIR /opt/src
COPY . .
RUN cargo install --path .

FROM debian:buster-slim
WORKDIR /opt
COPY --from=cargo-build /usr/local/cargo/bin/oauth2-mock-server /opt/oauth2-mock-server
CMD ["/opt/oauth2-mock-server"]
